import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,RouterModule
  ],
  declarations: [PageNotFoundComponent],
  exports: [
    PageNotFoundComponent
  ]
})
export class PageNotFoundModule {
}
