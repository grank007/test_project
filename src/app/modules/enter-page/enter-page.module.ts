import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnterPageComponent } from './enter-page/enter-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule
  ],
  declarations: [EnterPageComponent],
  exports: [
    EnterPageComponent
  ]
})
export class EnterPageModule {
}
