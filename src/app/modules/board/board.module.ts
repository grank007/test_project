import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component/board.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BoardService } from './board.service';
import { MockedService } from './mocked.service';
import { CompanyComponent } from './company/company.component';
import { SlotComponent } from './slot/slot.component';
import { ReservationComponent } from './reservation/reservation.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
  ],
  providers: [BoardService, MockedService],
  declarations: [BoardComponent, CompanyComponent, SlotComponent, ReservationComponent],
  exports: [BoardComponent]
})
export class BoardModule {
}
