import { Component, Input, OnInit } from '@angular/core';
import { TimeSlotFull } from '../board.service';

@Component({
  selector: 'app-slot',
  templateUrl: './slot.component.html',
  styleUrls: ['./slot.component.css']
})
export class SlotComponent implements OnInit {
  @Input() timeSlot: TimeSlotFull = null;

  constructor() {
  }

  ngOnInit() {
  }

}
