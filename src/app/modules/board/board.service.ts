import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MockedService } from './mocked.service';
import { UUID } from 'angular2-uuid';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  public dataCompaniesTime: CompanyInfo[] = [];
  public parsedCompaniesDates;

  constructor(private http: HttpClient, private mockedService: MockedService) {

    this.dataCompaniesTime = mockedService.getCompanies();
    this.buildDateForShow();
  }

  getOriginalDate() {
    return this.dataCompaniesTime;
  }

  getParsedDate() {
    return this.parsedCompaniesDates;
  }

  buildDateForShow() {
    this.parsedCompaniesDates = [...this.dataCompaniesTime];
    for (const company of this.parsedCompaniesDates) {
      this.groupByDate(company);
    }
    this.parsedCompaniesDates;
  }

  /*  filterDataSlotByTime() {
      for (const company of this.dataCompaniesTime) {
        company.time_slots = company.time_slots.sort(this.sortByDate);
        this.groupByDate(company);
      }
    }*/

  groupByDate(company) {
    company.parsedTimesSlots = [];
    for (const timeSlot of company.time_slots) {
      const date = new Date(timeSlot.start_time);
      const localDateSting = date.toLocaleDateString('en-US');
      const item = {
        fullDate: localDateSting,
        time_slots: [],
      };
      console.log(item);
      this.updateFullDateListForCompany(item, timeSlot, company);
    }
  }

  updateFullDateListForCompany(item: ParsedTimeSlots, timeSlot: TimeSlot, company: CompanyInfoParsed) {
    const uniqueSlot = company.parsedTimesSlots.find((parsedTimesSlot) => {
      return parsedTimesSlot.fullDate === item.fullDate;
    });
    const timeSloteFullDate: TimeSlotFull = {
      companyId: company.id,
      id: UUID.UUID(),
      start_time: timeSlot.start_time,
      end_time: timeSlot.end_time
    };
    if (uniqueSlot) {
      uniqueSlot.time_slots.push(timeSloteFullDate);
    } else {
      company.parsedTimesSlots.push(item);
    }
  }

}

export interface CompanyInfo {
  id: number;
  name: string;
  time_slots: TimeSlot[];
  type: string;
}

export interface CompanyInfoParsed {
  id: number;
  name: string;
  time_slots: TimeSlot[];
  type: string;
  parsedTimesSlots: ParsedTimeSlots[];
}

export interface TimeSlot {
  start_time: string;
  end_time: string;
}

export interface ParsedTimeSlots {
  fullDate: string;
  time_slots: TimeSlot[];
}

export interface TimeSlotFull {
  companyId: number;
  id: string;
  start_time: string;
  end_time: string;
}
