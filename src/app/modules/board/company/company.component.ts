import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { CompanyInfoParsed, TimeSlotFull } from '../board.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnChanges {
  @Input() company: CompanyInfoParsed = null;
  @Input() currentlySelectedSlote: TimeSlotFull = null;
  @Output() selectTimeSlote = new EventEmitter();
  public selectedTimeSlote: TimeSlotFull = null;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.selectedTimeSlote = this.currentlySelectedSlote;
  }

  setSelectedState(selectedSlot: TimeSlotFull) {
    this.selectDeselectTimeSlote(selectedSlot);
  }

  selectDeselectTimeSlote(selectedSlot: TimeSlotFull) {
    if (this.selectedTimeSlote && this.selectedTimeSlote.id === selectedSlot.id) {
      this.selectedTimeSlote = null;
    } else {
      this.selectedTimeSlote = selectedSlot;
      this.selectTimeSlote.next(this.selectedTimeSlote);
    }
  }
}
