import { Component, Input, OnInit } from '@angular/core';
import { TimeSlotFull } from '../board.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
  @Input() selectedTimeSlote: TimeSlotFull = null;
  @Input() companyId;

  constructor() {
  }

  ngOnInit() {
  }

}
