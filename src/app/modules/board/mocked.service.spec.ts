import { TestBed, inject } from '@angular/core/testing';

import { MockedService } from './mocked.service';

describe('MockedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockedService]
    });
  });

  it('should be created', inject([MockedService], (service: MockedService) => {
    expect(service).toBeTruthy();
  }));
});
