import { Component, OnInit } from '@angular/core';
import { BoardService, CompanyInfo, CompanyInfoParsed, ParsedTimeSlots, TimeSlot, TimeSlotFull } from '../board.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  public dataCompaniesTime: CompanyInfoParsed[] = [];
  public selectedTimeSlote: TimeSlotFull = null;

  constructor(private boardService: BoardService) {
  }

  ngOnInit() {
    this.dataCompaniesTime = this.boardService.getParsedDate();
  }

  setSelectedtimeSlot(selectedSlote) {
    this.selectedTimeSlote = selectedSlote;
  }

  /*
    setSelectedState(selectedSlot: TimeSlotFull) {
      this.selectDeselectTimeSlote(selectedSlot);
    }

    selectDeselectTimeSlote(selectedSlot: TimeSlotFull) {
      if (this.selectedTimeSlote && this.selectedTimeSlote.id === selectedSlot.id) {
        this.selectedTimeSlote = null;
      } else {
        this.selectedTimeSlote = selectedSlot;
      }
    }*/
}
