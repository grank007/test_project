import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { BoardComponent } from './modules/board/board.component/board.component';
import { PageNotFoundComponent } from './modules/page-not-found/page-not-found/page-not-found.component';
import { PageNotFoundModule } from './modules/page-not-found/page-not-found.module';
import { BoardModule } from './modules/board/board.module';
import { EnterPageModule } from './modules/enter-page/enter-page.module';
import { EnterPageComponent } from './modules/enter-page/enter-page/enter-page.component';
import {HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  {
    path: 'board',
    component: BoardComponent,
  },
  {
    path: '',
    component: EnterPageComponent,
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true} // <-- debugging purposes only
    ),
    EnterPageModule,
    PageNotFoundModule,
    BoardModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
